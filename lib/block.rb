require "faastruby/version"
require "yaml"

module AutoinitRuby

  # This class handles the initialization of the contents
  # of a `handler.rb` from a very simple template.
  # It will also generate the `faastruby.yml` config for
  # the block.
  class Block
    attr_accessor :name,
                  :runtime
    def initialize(event)
      @handler_path = event.path
      @block_path = File.dirname(@handler_path)
      @name = name_from(@block_path.dup)
      @runtime = "ruby:2.6" # Defaults to Ruby 2.6
    end

    # This method gets called to perform the initialization
    def init
      write_yaml
      write_handler
    end

    private

    # Extracts the block name from the path.
    # The argument `path` is always in the format:
    # `[blocks_dir]/for/bar`
    def name_from(path)
      path.slice!(
        "#{Faast::Watchdog::Project.config["functions_dir"]}/"
      )
      path
    end

    # Template used to generate a new `faastruby.yml` file.
    def yaml_hash
      {
        'cli_version' => FaaStRuby::VERSION,
        'name' => name,
        'runtime' => runtime
      }
    end

    # Merges an existing `faastruby.yml` file with the new one.
    # The new configuration will overwrite the old one.
    def merge_yaml(hash, yaml_file)
      current_yaml = YAML.load(File.read("#{@block_path}/faastruby.yml"))
      new_config = current_yaml.merge(hash)
      File.write(yaml_file, new_config.to_yaml)
    end

    # Write the `faastruby.yml` file to the block's directory.
    def write_yaml
      yaml_file = "#{@block_path}/faastruby.yml"
      if File.file?(yaml_file)
        merge_yaml(yaml_hash, yaml_file)
      else
        File.write(yaml_file, yaml_hash.to_yaml)
      end
    end

    # Writes the block's `handler.rb` initial template.
    def write_handler
      content = "def handler(event)\n  # Write code here\n  \nend"
      file = "#{@block_path}/handler.rb"
      if File.size(file) > 0
        puts "New Ruby function '#{@name}' detected."
      else
        File.write(file, content)
        puts "New Ruby function '#{@name}' initialized."
      end
    end
  end
end