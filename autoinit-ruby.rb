require_relative "lib/block"

# Declaring the extension
extension = Faast::Watchdog::Extension.new(
  name: "autoinit-ruby",
  description: "This extension initializes Ruby blocks automatically when it detects a file 'handler.rb'",
  version: "0.1.0"
)

# Getting the blocks directory from the project configuration
blocks_dir = Faast::Watchdog::Project.config["functions_dir"]

# Watch for events on any file inside `blocks_dir` with name
# `handler.rb`
extension.watch("#{blocks_dir}/**/handler.rb") do

  # When a file `handler.rb` gets added to any folder.
  on :added do |event|
    block = AutoinitRuby::Block.new(event)
    block.init
  end

  # When a file `handler.rb` is modified.
  # on :modified do |event|
  #   # Code here
  # end

  # When a file `handler.rb` is removed.
  # on :removed do |event|
  #   # Code here
  # end

end
